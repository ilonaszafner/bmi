package ilonaszafner.bmi.model;

public class Calculator {

    public boolean isMale;
    public int weight;
    public int growth;

    public Calculator() {
    }

    public Calculator(boolean isMale, int weight, int growth) {
        this.isMale = isMale;
        this.weight = weight;
        this.growth = growth;
    }

    public boolean isMale() {
        return isMale;
    }

    public void setMale(boolean male) {
        isMale = male;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getGrowth() {
        return growth;
    }

    public void setGrowth(int growth) {
        this.growth = growth;
    }

}
