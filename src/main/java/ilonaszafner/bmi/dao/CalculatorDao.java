package ilonaszafner.bmi.dao;

import ilonaszafner.bmi.model.Calculator;

import static jdk.nashorn.internal.objects.NativeMath.round;

public class CalculatorDao extends Calculator {
    double BMI;

    public double countBMI(boolean isMale, int weight, int growth) {
        BMI = (weight) / (growth * (double) growth);
        BMI = BMI * 10000;
        return BMI;
    }

    public String resultBMI() {
        if (BMI < 16) {
            return "Stanowi to wygłodzenie!";
        } else if (BMI >= 16 && BMI <= 16.99) {
            return "Wychudzenie";
        } else if (BMI >= 17 && BMI <= 18.49) {
            return "Niedowaga";
        } else if (BMI >= 18.5 && BMI <= 24.99) {
            return "Waga prawidłowa";
        } else if (BMI >= 25 && BMI <= 29.99) {
            return "Nadwaga";
        } else if (BMI >= 30 && BMI <= 34.99) {
            return "otyłość I stopnia";
        } else if (BMI >= 35 && BMI <= 39.99) {
            return "otyłość II stopnia, twz. otyłość kliniczna";
        } else {
            return "otyłość III stopnia";
        }

    }
}