package ilonaszafner.bmi.controller;


import ilonaszafner.bmi.dao.CalculatorDao;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@org.springframework.stereotype.Controller
public class Controller {

    CalculatorDao calculator = new CalculatorDao();

    @GetMapping("/")
    public String home() {
        return "home";
    }

    @GetMapping("/result")
    public String result(@RequestParam Boolean option, int weight, int growth, ModelMap modelMap) {
        modelMap.put("result", calculator.countBMI(option, weight, growth));
        modelMap.put("resultBMI", calculator.resultBMI());
        return "result";
    }


}
